package com.tiimipaiva.calculator;

import com.tiimipaiva.TiimipaivaApplicationTests;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ReturnSmallestTest extends TiimipaivaApplicationTests {

    @Autowired
    ReturnSmallest returnSmallest;

    @Test
    public void is_value_smallest(){
        var values = List.of(3,5,6,1,2,8);
        var minValue = returnSmallest.findSmallestValueInList(values);

        assertThat(minValue, is(equalTo(1)));


    }
}
