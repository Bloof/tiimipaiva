package com.tiimipaiva.calculator;

import com.tiimipaiva.enums.MoneyType;
import lombok.Builder;

@Builder
public class ChangeResult {
    Integer coins;
    MoneyType type;
}
