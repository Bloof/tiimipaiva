package com.tiimipaiva.calculator;

import com.tiimipaiva.enums.MoneyType;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ChangeCalculator {

    public Map<MoneyType, Integer> calculateChange(BigDecimal changeAmount) {

        var change = new HashMap<MoneyType, Integer>();

        for (MoneyType type : MoneyType.values()) {
            var coins = calculateCoins(changeAmount, type.getAmount());
            changeAmount = changeAmount.subtract(type.getAmount().multiply(BigDecimal.valueOf(coins)));
            change.put(type, coins);
        }

        return change
                .entrySet()
                .stream()
                .filter(this::isCoinsAvailable)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private boolean isCoinsAvailable(Map.Entry<MoneyType, Integer> value) {
        return value.getValue() != 0;
    }

    private int calculateCoins(BigDecimal moneyLeft, BigDecimal moneyTypeAmount) {
        var coins = 0;
        for (var i = moneyLeft; i.compareTo(moneyTypeAmount) >= 0; i = i.subtract(moneyTypeAmount)) {
            coins++;
        }
        return coins;
    }

}
