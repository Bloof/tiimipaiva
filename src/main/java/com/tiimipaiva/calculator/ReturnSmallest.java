package com.tiimipaiva.calculator;

import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReturnSmallest {

    public int findSmallestValueInList(List<Integer> values) {

       return values.stream().sorted(this::compare).findFirst().get();
    }

    public int findIntegerMissingFromFirstList(List<Integer> first, List<Integer> second) {
        var sortedFirst = first.stream().sorted(this::compare);
        var sortedSecond = first.stream().sorted(this::compare);
        return 1;
    }

    private int compare(int a, int b) {
        return a-b;
    }

}
